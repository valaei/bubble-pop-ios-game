//
//  ScoreBoardTableViewCell.swift
//  bubblePop
//
//  Created by Arian Valaei on 29/4/21.
//

import UIKit

class ScoreBoardTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

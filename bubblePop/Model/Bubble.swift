//
//  Bubble.swift
//  bubblePop
//
//  Created by Arian Valaei on 30/4/21.
//

import UIKit

class Bubble: UIButton {
    
    let scoreTable = ["red": 1, "pink": 2,"green": 5, "blue": 8, "black": 10]
    let distributionArray: [Double] = [0.4, 0.3, 0.15, 0.10, 0.05]
    let colorArray: [String] = ["red", "pink", "green", "blue", "black"]
    let screenWidth: Int = Int(UIScreen.main.bounds.width)
    let screenHeight: Int = Int(UIScreen.main.bounds.height)
    let radius: Int = 75
    static var bubbleCount : Int = 0
    static var speed : Int = 5
    var point: Int = 0
    var color: String = ""
    var xPosition: Int = 0
    var yPosition: Int = 0
    var timer = Timer()

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        color = colorArray[randomNumber(probabilities: distributionArray)]
        switch color {
        case "red":
            self.backgroundColor = .red
        case "pink":
            self.backgroundColor = UIColor(red: 0.98, green: 0.85, blue: 0.86, alpha: 1)
        case "green":
            self.backgroundColor = .green
        case "blue":
            self.backgroundColor = .blue
        case "black":
            self.backgroundColor = .black
        default:
            break
        }
        self.point = scoreTable[color]!
        self.layer.zPosition = -1;
        Bubble.bubbleCount += 1;
        timer = Timer.scheduledTimer(withTimeInterval: 0.02, repeats: true) {
            timer in
            self.move()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func position(section: Int) {
        yPosition = Int.random(in:  screenHeight...screenHeight + radius)
        switch section {
        case 0:
            xPosition = Int.random(in:  0...(screenWidth / 4) - radius)
        case 1:
            xPosition = Int.random(in:  (screenWidth / 4)...((screenWidth / 2) - radius))
        case 2:
            xPosition = Int.random(in:  (screenWidth / 2)...((3 * screenWidth / 4) - radius))
        case 3:
            xPosition = Int.random(in:  (3 * screenWidth / 4)...(screenWidth  - radius))
        default:
            break;
        }
        self.frame = CGRect(x: xPosition, y: yPosition, width: radius, height: radius)
        self.layer.cornerRadius = 0.5 * self.bounds.size.width
    }
    
    func move() {
        yPosition -= Bubble.speed;
        self.frame = CGRect(x: xPosition, y: yPosition, width: radius, height: radius)
        if(yPosition < -radius) {
            self.removeFromSuperview()
            timer.invalidate()
            Bubble.bubbleCount -= 1;
        }
    }
   
    func destroy() {
        let springAnimation = CABasicAnimation(keyPath: "transform.scale")
        springAnimation.duration = 0.2
        springAnimation.fromValue = 1
        springAnimation.toValue = 0
        springAnimation.repeatCount = 0
        layer.add(springAnimation, forKey: nil)
        var _:Timer = Timer.scheduledTimer(timeInterval: 0.18, target: self, selector: #selector(self.removeFromSuperview), userInfo: nil, repeats: false)
    }
    
    func randomNumber(probabilities: [Double]) -> Int {

        let sum = probabilities.reduce(0, +)
        let rnd = Double.random(in: 0.0 ..< sum)
        var accum = 0.0
        for (i, p) in probabilities.enumerated() {
            accum += p
            if rnd < accum {
                return i
            }
        }
        return (probabilities.count - 1)
    }
    
}

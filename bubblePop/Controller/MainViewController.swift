//
//  MainViewController.swift
//  bubblePop
//
//  Created by Arian Valaei on 29/4/21.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var nameText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true);
        self.navigationController?.setNavigationBarHidden(false, animated: true)

    }
    


    @IBAction func startPressed(_ sender: Any) {
        if nameText.text != "" {
            UserDefaults.standard.set(nameText.text, forKey: "newPlayer")
            self.performSegue(withIdentifier: "gameSegue", sender: self)
        } else {
            let alert = UIAlertController(title: "", message: "Enter Your Name!", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    

    


}

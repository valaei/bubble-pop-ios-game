//
//  SettingViewController.swift
//  bubblePop
//
//  Created by Arian Valaei on 29/4/21.
//

import UIKit

class SettingViewController: UIViewController {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var maxBubbleLabel: UILabel!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var maxBubbleSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true);
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let timeSetting = UserDefaults.standard.object(forKey: "timeSetting") as? Int ?? 60
        let maxBubbleSetting = UserDefaults.standard.object(forKey: "maxBubbleSetting") as? Int ?? 15
        
        timeSlider.value = Float(timeSetting)
        timeLabel.text = String(timeSetting)
        maxBubbleSlider.value = Float(maxBubbleSetting)
        maxBubbleLabel.text = String(maxBubbleSetting)
    }
    
    
    @IBAction func donePressed(_ sender: Any) {
        UserDefaults.standard.set(Int(timeLabel.text!), forKey: "timeSetting")
        UserDefaults.standard.set(Int(maxBubbleLabel.text!), forKey: "maxBubbleSetting")
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func timeValueChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        timeLabel.text = String(currentValue)
    }
    
    @IBAction func maxBubbleValueChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        maxBubbleLabel.text = String(currentValue)
    }

}

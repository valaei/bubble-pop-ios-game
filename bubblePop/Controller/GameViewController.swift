//
//  GameViewController.swift
//  bubblePop
//
//  Created by Arian Valaei on 29/4/21.
//

import UIKit

class GameViewController: UIViewController {

    var timeSetting: Int = 0
    var maxBubbleSetting: Int = 0
    var highScore: Double = 0
    var score: Double = 0
    var playerName: String = ""
    var lastBubblePressed: String = ""
    var timer = Timer()
    var timer2 = Timer()


    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var highScoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        timeSetting = UserDefaults.standard.object(forKey: "timeSetting") as? Int ?? 60
        maxBubbleSetting = UserDefaults.standard.object(forKey: "maxBubbleSetting") as? Int ?? 15
        highScore = UserDefaults.standard.object(forKey: "highScore") as? Double ?? 0
        playerName = UserDefaults.standard.string(forKey: "newPlayer")!
        timeLabel.text = String(timeSetting)
        highScoreLabel.text = String(highScore)
        Bubble.bubbleCount = 0
        Bubble.speed = 5
        
        timer = Timer.scheduledTimer(withTimeInterval: 0.6, repeats: true) {
            timer in
            self.generateBubble()
        }
        timer2 = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) {
            timer in
            self.counting()
        }

    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func counting() {
        var remainingTime = Int(timeLabel.text!)
        remainingTime! -= 1
        if (remainingTime! <= 0) {
            timer2.invalidate()
            timer.invalidate()

            if(highScore == score) {
                UserDefaults.standard.set(highScore, forKey: "highScore")
            }
            
            var scoreboard = UserDefaults.standard.object(forKey: "scoreboard") as? [[String]] ?? []
            scoreboard.append([playerName, String(score)])
            UserDefaults.standard.set(scoreboard, forKey: "scoreboard")
            self.performSegue(withIdentifier: "scoreboardSegue", sender: self)
        } else {
            timeLabel.text = String(remainingTime!)
        }
        if (remainingTime! % 10 == 0) {
            Bubble.speed += 1
        }
    }

    @objc func generateBubble() {
        let creationChance: Float = 0.6
        let maxBubbleSetting = UserDefaults.standard.object(forKey: "maxBubbleSetting") as? Int ?? 15
        for section in 0...3 {
            if (Float.random(in: 0.0 ..< 1.0) < creationChance && Bubble.bubbleCount < maxBubbleSetting) {
                let bubble = Bubble()
                bubble.addTarget(self, action: #selector(bubblePressed), for: .touchUpInside)
                bubble.position(section: section)
                self.view.addSubview(bubble)
            }
        }
    }
    
    @IBAction func bubblePressed(_ sender: UIButton) {
        let bubble = sender as! Bubble
        let color = bubble.color
        if(lastBubblePressed == color) {
            score += 1.5 * Double(bubble.point)
        } else {
            score += Double(bubble.point)
        }
        if (score > highScore) {
            highScore = score
            highScoreLabel.text = String(highScore)
        }
        scoreLabel.text = String(score)
        lastBubblePressed = color
        bubble.destroy()
    }

}

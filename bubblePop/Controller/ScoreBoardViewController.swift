//
//  ScoreBoardViewController.swift
//  bubblePop
//
//  Created by Arian Valaei on 29/4/21.
//

import UIKit

class ScoreBoardViewController: UIViewController {
    

    @IBOutlet weak var scoreBoardTableView: UITableView!
    
    var scoreboard: [[String]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true);
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let nib = UINib(nibName: "ScoreBoardTableViewCell", bundle: nil)
        scoreBoardTableView.register(nib, forCellReuseIdentifier: "ScoreBoardTableViewCell")
        scoreBoardTableView.delegate = self
        scoreBoardTableView.dataSource = self
        scoreboard = UserDefaults.standard.object(forKey: "scoreboard") as? [[String]] ?? []
        if (scoreboard.count > 1) {
            scoreboard.sort { (Double($0[1])!) > (Double($1[1])!) }
        }
    }
    
    @IBAction func homePressed(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)

    }
    
}

extension ScoreBoardViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

extension ScoreBoardViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoreboard.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = scoreBoardTableView.dequeueReusableCell(withIdentifier: "ScoreBoardTableViewCell", for: indexPath) as! ScoreBoardTableViewCell
        
        cell.nameLabel.text = scoreboard[indexPath.row][0]
        cell.scoreLabel.text = scoreboard[indexPath.row][1]

        return cell
    }
    
}
